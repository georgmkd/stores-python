from flask import Flask, jsonify
from flask_restful import Api
from flask_jwt import JWT
from flask_cors import CORS

from security import authenticate, identity
from resources.user import UserRegister
from resources.item import Item, ItemList, ItemPost
from resources.store import Store, StoreList

app = Flask(__name__)
CORS(app)

app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root@localhost/stores'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.secret_key = 'george-laz'
api = Api(app)

@app.before_first_request
def create_tables():
    db.create_all()

jwt = JWT(app, authenticate, identity) # /auth

@jwt.auth_response_handler
def customized_response_handler(access_token, identity):
    return jsonify({
 'access_token':
access_token.decode('utf-8'),
 'role': identity.role
 })

api.add_resource(Store, '/store/<string:name>')
api.add_resource(ItemPost, '/item')
api.add_resource(Item, '/item/<string:name>')
api.add_resource(ItemList, '/items')
api.add_resource(StoreList, '/stores')
api.add_resource(UserRegister, '/register')


if __name__ == '__main__':
    from db import db
    db.init_app(app)
    app.run( port=5000, debug=True, threaded=True)
