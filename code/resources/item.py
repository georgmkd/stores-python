from flask_restful import Resource, reqparse
from flask_jwt import jwt_required, current_identity
from models.item import ItemModel

class Item(Resource):

    parser = reqparse.RequestParser()
    parser.add_argument('name',
                        type=float,
                        required=True,
                        help="Name field cannot be left blank!"
                        )
    parser.add_argument('price',
                        type=float,
                        required=True,
                        help="Price field cannot be left blank!"
                        )
    parser.add_argument('store_id',
                        type=int,
                        required=True,
                        help="Select the store for the product!"
                        )

    @jwt_required()
    def get(self, name):
        user = current_identity.role
        if (user=="user_superadmin"):
            item = ItemModel.find_by_name(name)
            if item:
                return item.json()
            return {'message': 'Item not found'}, 404
        else:
            return {'message' : 'User Unauthorized'},401


    def delete(self, name):
        item = ItemModel.find_by_name(name)
        if item:
            item.delete_from_db()

        return {'message': 'Item Deleted'}


    def put(self, name):

        data = Item.parser.parse_args()

        item = ItemModel.find_by_name(name)

        if item is None:
            item = ItemModel(name, **data) # **data = data['price'], data['store_id']
        else:
            item.price = data['price']

        item.save_to_db()

        return item.json()


class ItemList(Resource):

    def get(self):
        return {'items': [item.json() for item in ItemModel.query.all()]} # current is LIST COMPREHENSION but, can be used using lambda - list(map(lambda x: x.json(), ItemModel.query.all()

    parser2 = reqparse.RequestParser()
    parser2.add_argument('page',
                        type=int,
                        required=True,
                        help="Page field cannot be left blank!"
                        )
    parser2.add_argument('per_page',
                        type=int,
                        required=True,
                        help="Per Page field cannot be left blank!"
                        )

    def post(self):
        data = ItemList.parser2.parse_args()
        page = data['page']
        per_page = data['per_page']
        total_items = len(ItemModel.query.all())
        total_pages = total_items/per_page
        if(total_items%per_page > 0):
            total_pages = total_pages + 1
        else:
            total_pages = total_pages
        if (page > total_pages):
            return {'message': 'There is no such page={}'.format(page)}, 400
        if (page <= 0 or per_page <= 0):
            return {'message': 'Page or Per_page values should be positive numbers'}, 400
        return [{'pagination':{'page': page, 'per_page': per_page, 'total_pages': total_pages, 'total_items': total_items}}, {'items': [item.json() for item in ItemModel.query.paginate(page=page, per_page=per_page).items]}] # current is LIST COMPREHENSION but, can be used using lambda - list(map(lambda x: x.json(), ItemModel.query.all()

class ItemPost(Resource):

    parser1 = reqparse.RequestParser()
    parser1.add_argument('name',
                        type=str,
                        required=True,
                        help="Name field cannot be left blank!"
                        )
    parser1.add_argument('price',
                        type=float,
                        required=True,
                        help="Price field cannot be left blank!"
                        )
    parser1.add_argument('store_id',
                        type=int,
                        required=True,
                        help="Select the store for the product!"
                        )
    def post(self):
        data = ItemPost.parser1.parse_args()
        if ItemModel.find_by_name(data['name']):
            return {'message': "An item with name '{}' already exists.".format(data['name'])}, 400
        if len(data['name']) < 2:
            return {"message": {"name": "An item name with minimum of 2 letters should be provided!"}}, 400



        item = ItemModel(data['name'], data['price'], data['store_id'])

        try:
            item.save_to_db()
        except:
            return {"message":"An error occured inserting the item."}, 500

        return item.json(), 201