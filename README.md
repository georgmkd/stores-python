Stores management platform for sorting items.

This is built with Flask, Flask-RESTful, Flask-JWT, and Flask-SQLAlchemy

1. Setup a mysql database with name stores
2. Install Python, Flask, Flask-RESTful, Flask-JWT, Flask-SQLAlchemy and other required libraries for the project to function as required in the command line when run app.py
3. Create a user with the /register
{
  "username": "admin",
  "password": "pass",
  "role": "user_superadmin"
}
4. Then after getting message "User created succesfully" go to /auth and fill out the message body

{
  "username": "admin",
  "password": "pass"
}

5. After authorization you are provided with access_token which enables the user based on his privileges to perform actions with the API's CRUD operations